package dbccompany.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import dbccompany.model.Fipe;
import dbccompany.model.Marca;
import dbccompany.model.Veiculo;
import dbccompany.service.FipeService;
import dbccompany.util.Calc;

@Named
@ConversationScoped
public class FipeAction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private FipeService fipeService;

	private List<Marca> marcas = new ArrayList<Marca>();
	private Marca marca;
	private List<Veiculo> veiculos = new ArrayList<Veiculo>();
	private Veiculo veiculo;
	private List<Veiculo> modelos = new ArrayList<Veiculo>();
	private Veiculo modelo;
	
//	private Fipe fipe;
	
	private List<Fipe> fipesModelo = new ArrayList<Fipe>(); 
	
	private List<String> alteracoesFipeModelo = new ArrayList<String>();

	@PostConstruct
	public void init() {
		if (marcas.isEmpty()) {
			marcas = fipeService.listarTodasMarcas();
		}
	}

	public void listarVeiculosPorMarca(){
		this.veiculos = fipeService.listarVeiculosPorMarca(marca);
	}

	public void listarVeiculosPorMarcaEModelo(){
		this.modelos = fipeService.listarVeiculosPorMarcaEModelo(veiculo);
	}

	public void pesquisarTabelaFipePorMarcaEModeloEAno(){
		modelos.forEach(mod ->{
			this.veiculo.setFipe_codigo(mod.getFipe_codigo());
			fipesModelo.add(fipeService.pesquisarTabelaFipePorMarcaEModeloEAno(veiculo));
		});
		fipesModelo.sort(Comparator.comparing(Fipe::getId).reversed());
		for (int i = 0; i < fipesModelo.size(); i++) {
			Fipe fipe = fipesModelo.get(i);
			if (i == 0) {
				alteracoesFipeModelo.add(" - Valor em "+fipe.getId()+" -> "+fipe.getPreco()+",");
			} else if (i > 0) {
				alteracoesFipeModelo.add(Calc.calculoPercentualAlteracao(fipe, fipesModelo.get(i-1)));
			}
		}
	}

	public void onRowSelect(SelectEvent event) {
		this.veiculo = (Veiculo) event.getObject();
		pesquisarTabelaFipePorMarcaEModeloEAno();
	}

	public void onRowUnselect(UnselectEvent event) {
		this.veiculo = null;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public List<Marca> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<Marca> marcas) {
		this.marcas = marcas;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public List<Veiculo> getModelos() {
		return modelos;
	}

	public void setModelos(List<Veiculo> modelos) {
		this.modelos = modelos;
	}

	public Veiculo getModelo() {
		return modelo;
	}

	public void setModelo(Veiculo modelo) {
		this.modelo = modelo;
	}

	public List<Fipe> getFipesModelo() {
		return fipesModelo;
	}

	public void setFipesModelo(List<Fipe> fipesModelo) {
		this.fipesModelo = fipesModelo;
	}

	public List<String> getAlteracoesFipeModelo() {
		return alteracoesFipeModelo;
	}

	public void setAlteracoesFipeModelo(List<String> alteracoesFipeModelo) {
		this.alteracoesFipeModelo = alteracoesFipeModelo;
	}


}