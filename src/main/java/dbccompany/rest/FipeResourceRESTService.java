package dbccompany.rest;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import dbccompany.model.Fipe;
import dbccompany.model.Marca;
import dbccompany.model.Veiculo;
import dbccompany.service.FipeService;

@Path("/fipe")
@RequestScoped
public class FipeResourceRESTService {

	@Inject
	private FipeService fipeService;
	
	@GET
	@Path("/marcas")
	public List<Marca> listarTodasMarcas(){
		return fipeService.listarTodasMarcas();
	}

	@GET
	@Path("/{id-marca}/veiculos")
	public List<Veiculo> listarVeiculosPorMarca(@PathParam("id-marca") String idMarca){
		return fipeService.listarVeiculosPorMarca(new Marca(idMarca));
	}
	
	@GET
	@Path("/{id-marca}/{id-modelo}/veiculos")
	public List<Veiculo> listarVeiculosPorMarcaEModelo(Veiculo veiculo){
		return fipeService.listarVeiculosPorMarcaEModelo(veiculo);
	}

	@GET
	@Path("/{id-marca}/{id-modelo}/{fipe-codigo}/fipe")
	public Fipe pesquisarTabelaFipePorMarcaEModeloEAno(Veiculo veiculo){
		return fipeService.pesquisarTabelaFipePorMarcaEModeloEAno(veiculo);
	}
	
	public static void main(String[] args) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		// parse the string
		BigDecimal bigDecimal;
		try {
			bigDecimal = (BigDecimal) decimalFormat.parse("109.046,00");
			System.out.println(bigDecimal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String str="109.046,00".replaceAll(",","");
		 BigDecimal bd=new BigDecimal(str);
		 System.out.println(bd);
		
	}
}
