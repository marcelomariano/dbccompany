package dbccompany.model;

import java.io.Serializable;

public class Veiculo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
			
	private Long id;
	private String fipe_marca;
	private String fipe_codigo;
	private String name;
	private String marca;
	private String key;
	private String fipe_name;
	
	public Veiculo() {
		// TODO Auto-generated constructor stub
	}
	
	public Veiculo(String marca, String modelo, String fipeCodigo) {
		// TODO Auto-generated constructor stub
		this.marca = marca;
		this.id = Long.getLong(modelo);
		this.fipe_codigo = fipeCodigo;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFipe_marca() {
		return fipe_marca;
	}
	public void setFipe_marca(String fipe_marca) {
		this.fipe_marca = fipe_marca;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getFipe_name() {
		return fipe_name;
	}
	public void setFipe_name(String fipe_name) {
		this.fipe_name = fipe_name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fipe_marca == null) ? 0 : fipe_marca.hashCode());
		result = prime * result
				+ ((fipe_name == null) ? 0 : fipe_name.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (fipe_marca == null) {
			if (other.fipe_marca != null)
				return false;
		} else if (!fipe_marca.equals(other.fipe_marca))
			return false;
		if (fipe_name == null) {
			if (other.fipe_name != null)
				return false;
		} else if (!fipe_name.equals(other.fipe_name))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", fipe_marca=" + fipe_marca + ", name="
				+ name + ", marca=" + marca + ", key=" + key + ", fipe_name="
				+ fipe_name + "]";
	}
	public String getFipe_codigo() {
		return fipe_codigo;
	}
	public void setFipe_codigo(String fipe_codigo) {
		this.fipe_codigo = fipe_codigo;
	}
	
	

}
