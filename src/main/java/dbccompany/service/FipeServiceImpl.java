package dbccompany.service;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;

import dbccompany.model.Fipe;
import dbccompany.model.Marca;
import dbccompany.model.Veiculo;

@Named
@Stateless
public class FipeServiceImpl implements FipeService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public List<Marca> listarTodasMarcas() {
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(FipeService.LISTAR_MARCAS_URL);
			ClientResponse response = webResource.accept(
					MediaType.APPLICATION_JSON).get(ClientResponse.class);
			if (response.getStatus() != Status.OK.getStatusCode()) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			Marca[] marcas = response.getEntity(Marca[].class);
			return Arrays.asList(marcas);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Veiculo> listarVeiculosPorMarca(Marca marca) {
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(FipeService.LISTAR_VEICULOS_POR_MARCA.replace("{id-marca}", marca.getId().toString()));
			ClientResponse response = webResource.accept(
					MediaType.APPLICATION_JSON).get(ClientResponse.class);
			if (response.getStatus() != Status.OK.getStatusCode()) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			Veiculo[] veiculos = response.getEntity(Veiculo[].class);
			return Arrays.asList(veiculos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Veiculo> listarVeiculosPorMarcaEModelo(Veiculo veiculo) {
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(FipeService.CONSULTAR_ANO_MODELO_POR_VEICULO
							.replace("{id-marca}", veiculo.getFipe_marca())
							.replace("{id-modelo}", veiculo.getId().toString()));
			ClientResponse response = webResource.accept(
					MediaType.APPLICATION_JSON).get(ClientResponse.class);
			if (response.getStatus() != Status.OK.getStatusCode()) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			Veiculo[] veiculos = response.getEntity(Veiculo[].class);
			return Arrays.asList(veiculos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Fipe pesquisarTabelaFipePorMarcaEModeloEAno(Veiculo veiculo) {
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(FipeService.CONSULTAR_TABELA_FIPE
							.replace("{id-marca}", veiculo.getFipe_marca())
							.replace("{id-modelo}", veiculo.getId().toString())
							.replace("{fipe-codigo}", veiculo.getFipe_codigo()));
			ClientResponse response = webResource.accept(
					MediaType.APPLICATION_JSON).get(ClientResponse.class);
			if (response.getStatus() != Status.OK.getStatusCode()) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			Fipe tabelaFipe = response.getEntity(Fipe.class);
			return tabelaFipe;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
