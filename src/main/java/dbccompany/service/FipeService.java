package dbccompany.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import dbccompany.model.Fipe;
import dbccompany.model.Marca;
import dbccompany.model.Veiculo;

@Local
public interface FipeService extends Serializable {

	public static final String LISTAR_MARCAS_URL 
								= "http://fipeapi.appspot.com/api/1/carros/marcas.json";

	public static final String LISTAR_VEICULOS_POR_MARCA 
								= "http://fipeapi.appspot.com/api/1/carros/veiculos/{id-marca}.json";

	public static final String CONSULTAR_ANO_MODELO_POR_VEICULO 
								= "http://fipeapi.appspot.com/api/1/carros/veiculo/{id-marca}/{id-modelo}.json";

	public static final String CONSULTAR_TABELA_FIPE
								= "http://fipeapi.appspot.com/api/1/carros/veiculo/{id-marca}/{id-modelo}/{fipe-codigo}.json";

	List<Marca> listarTodasMarcas();

	List<Veiculo> listarVeiculosPorMarca(Marca marca);
	
	List<Veiculo> listarVeiculosPorMarcaEModelo(Veiculo veiculo);
	
	Fipe pesquisarTabelaFipePorMarcaEModeloEAno(Veiculo veiculo);

}