package dbccompany.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import dbccompany.model.Fipe;

public class Calc {

	public static String calculoPercentualAlteracao(Fipe f1, Fipe f2 ){
		BigDecimal fipe1 = trasnformaEmBigDecimal(f1);
		BigDecimal fipe2 = trasnformaEmBigDecimal(f2);

		BigDecimal diferenca = fipe1.subtract(fipe2);

		BigDecimal percentual = diferenca.multiply(BigDecimal.valueOf(fipe2.doubleValue()/100));
		
		String value = "- Valor em "+f2.getAno_modelo()+" -> "+f2.getPreco()+
				" alteração de R$"+diferenca+" ("+percentual+"%) em relação a "+f1.getAno_modelo()+",";
		return value;
	}

	private static BigDecimal trasnformaEmBigDecimal(Fipe fipe){
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		BigDecimal bigDecimal;
		try {
			bigDecimal = (BigDecimal) decimalFormat.parse(fipe.getPreco().replace("R$", ""));
			return bigDecimal;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
